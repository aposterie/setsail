type MessageBase = {
  id: string
  senderType: "agent" | "user"
  createdAt: number
}
type Text = MessageBase & {
  contentType: "text"
  content: {
    text: string
  }
}
type Image = MessageBase & {
  contentType: "image"
  content: {
    url: string
  }
}
type Attachment = MessageBase & {
  contentType: "attachment"
  content: {
    file: string
  }
}
export type Message = Text | Image | Attachment
