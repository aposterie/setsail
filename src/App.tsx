import styled from "@emotion/styled"
import React from "react"
import "./App.css"
import { MessageView } from "./components/Message"
/** Conversation Data */
import conversationData from "./fixtures/conversation"

const Convo = styled.div`
  max-width: 800px;
`

function App() {
  return (
    <div className="App">
      <Convo>
        {conversationData
          .slice()
          .reverse()
          .map(message => (
            <MessageView key={message.id} message={message} />
          ))}
      </Convo>
    </div>
  )
}

export default App
