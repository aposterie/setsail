import React from "react"
import { Message } from "../types"
import { AttachmentMessage } from "./AttachmentMessage"
import { ImageMessage } from "./ImageMessage"
import { TextMessage } from "./TextMessage"

interface MessageViewProps {
  message: Message
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function assertNever(_value: never) {}

export const MessageView: SFC<MessageViewProps> = ({ message }) => {
  switch (message.contentType) {
    case "text":
      return <TextMessage message={message} />
    case "image":
      return <ImageMessage message={message} />
    case "attachment":
      return <AttachmentMessage message={message} />
    default:
      assertNever(message)
      return null
  }
}
