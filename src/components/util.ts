export const matchAgent = (isAgent: string, isUser: string) => ({
  sender,
}: {
  sender: "agent" | "user"
}) => (sender === "agent" ? isAgent : isUser)

export function downloadFile(link: string) {
  const a = document.createElement("a")
  a.download = ""
  a.href = link
  a.style.display = "none"
  document.body.appendChild(a)
  a.click()
  a.remove()
}
