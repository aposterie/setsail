import React from "react"
import styled from "@emotion/styled"
import { matchAgent } from "./util"

interface BaseMessageProps {
  children: React.ReactNode
  timestamp: number
  sender: "agent" | "user"
}

const borderRadius = (small: number, big: number) =>
  matchAgent(
    `${small}px ${big}px ${big}px ${small}px`,
    `${big}px ${small}px ${small}px ${big}px`
  )

const Block = styled.div<BaseMessageProps>`
  float: ${matchAgent("left", "right")};
  width: 100%;

  &:not(:last-of-type) {
    margin-bottom: 10px;
  }
`

const Container = styled.div<BaseMessageProps>`
  background: ${matchAgent("#fff", "#295e7f")};
  color: ${matchAgent("#000", "#fff")};
  float: ${matchAgent("left", "right")};
  border: 1px solid #e3e9ef;
  border-radius: ${borderRadius(6, 12)};
  padding: 10px 15px;
  max-width: 25em;
`

const Timestamp = styled.div<BaseMessageProps>`
  color: ${matchAgent("#8193ac", "#afd2f4")};
  font-size: 14px;
  margin: 10px 0 0;
`

export const BaseMessage: SFC<BaseMessageProps> = ({ children, ...props }) => (
  <Block
    {...props}
    style={
      // Hack to make texts look better on dark background
      props.sender === "user"
        ? {
            WebkitFontSmoothing: "antialiased",
          }
        : {}
    }
  >
    <Container {...props}>
      {children}
      <Timestamp {...props}>
        {new Date(props.timestamp * 1000).toLocaleString()}
      </Timestamp>
    </Container>
  </Block>
)
