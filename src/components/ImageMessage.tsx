import styled from "@emotion/styled"
import React from "react"
import { Image } from "../types"
import { BaseMessage } from "./BaseMessage"

interface ImageMessageProps {
  message: Image
}

const Img = styled.img`
  max-width: 100%;
  border-radius: 10px;
`

export const ImageMessage: SFC<ImageMessageProps> = ({ message }) => (
  <BaseMessage sender={message.senderType} timestamp={message.createdAt}>
    <Img src={message.content.url} />
  </BaseMessage>
)
