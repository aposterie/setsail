import React from "react"
import { Text } from "../types"
import { BaseMessage } from "./BaseMessage"

interface TextMessageProps {
  message: Text
}

export const TextMessage: SFC<TextMessageProps> = ({ message }) => (
  <BaseMessage sender={message.senderType} timestamp={message.createdAt}>
    {message.content.text}
  </BaseMessage>
)
