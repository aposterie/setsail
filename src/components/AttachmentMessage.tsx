import styled from "@emotion/styled"
import React from "react"
import { Attachment } from "../types"
import { BaseMessage } from "./BaseMessage"
import download from "./download.svg"
import { downloadFile, matchAgent } from "./util"

interface AttachmentMessageProps {
  message: Attachment
}

const Link = styled.div`
  display: flex;
  max-width: 100%;
  border-radius: 10px;
  a {
    color: inherit;
  }
`
const DownloadButton = styled.img<{ sender: "agent" | "user" }>`
  align-self: center;
  width: 1.6em;
  height: 1.6em;
  margin: 5px;
  margin-right: 10px;
  filter: ${matchAgent("none", "invert(1)")};
`
const DownloadButtonContainer = styled.div`
  display: grid;
  cursor: pointer;
`

export const AttachmentMessage: SFC<AttachmentMessageProps> = ({ message }) => (
  <BaseMessage sender={message.senderType} timestamp={message.createdAt}>
    <Link>
      <DownloadButtonContainer>
        <DownloadButton
          sender={message.senderType}
          src={download}
          onClick={() => downloadFile(message.content.file)}
        />
      </DownloadButtonContainer>
      <a href={message.content.file}>{message.content.file}</a>
    </Link>
  </BaseMessage>
)
